Source: yabause
Section: otherosfs
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Evgeni Golov <evgeni@debian.org>,
 Matt Arnold <adeodatus@sdf.org>
Build-Depends:
 cmake,
 debhelper-compat (= 12),
 freeglut3-dev | libglut-dev,
 libgl1-mesa-dev | libgl-dev,
 libglib2.0-dev,
 libglew-dev,
 libglu1-mesa-dev | libglu-dev,
 libmini18n-dev,
 libopenal-dev,
 libqt5opengl5-dev,
 libsdl2-dev,
 pkg-config,
 qtbase5-dev,
 qtmultimedia5-dev
Standards-Version: 4.4.0
Rules-Requires-Root: no
Homepage: https://yabause.org/
Vcs-Git: https://salsa.debian.org/games-team/yabause.git
Vcs-Browser: https://salsa.debian.org/games-team/yabause

Package: yabause
Architecture: all
Depends:
 yabause-qt,
 ${misc:Depends}
Description: beautiful and under-rated Saturn emulator
 Yabause is a Sega Saturn emulator. It has the following features:
  * booting games from CD-ROM and ISO files
  * booting games with either an emulated or original BIOS
  * screenshot support
  * savegame backups
  * cheat system
  * fullscreen playing
  * multiple debugging options
  * joystick support
  * region select
 .
 This package is a metapackage, which installs an available
 version of Yabause.

Package: yabause-common
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends}
Recommends:
 yabause-qt
Description: beautiful and under-rated Saturn emulator - common files
 Yabause is a Sega Saturn emulator. It has the following features:
  * booting games from CD-ROM and ISO files
  * booting games with either an emulated or original BIOS
  * screenshot support
  * savegame backups
  * cheat system
  * fullscreen playing
  * multiple debugging options
  * joystick support
  * region select
 .
 This package contains arch-independend files like images and translations.

Package: yabause-qt
Architecture: any
Depends:
 yabause-common (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: beautiful and under-rated Saturn emulator - Qt port
 Yabause is a Sega Saturn emulator. It has the following features:
  * booting games from CD-ROM and ISO files
  * booting games with either an emulated or original BIOS
  * screenshot support
  * savegame backups
  * cheat system
  * fullscreen playing
  * multiple debugging options
  * joystick support
  * region select
 .
 This package contains the Qt version of Yabause.
